// Require index.html and index.css so it gets copied to dist by webpack loaders
require("./index.html");
require("./index.css");

var Elm = require("./Counter/Counter.elm");
var mountNode = document.getElementById("app-root");

var app = Elm.Counter.embed(mountNode);
