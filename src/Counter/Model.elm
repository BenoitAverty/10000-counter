module Model exposing (..)

-- Imports
import Array exposing (Array, map)
import List exposing (filter, head)

import Utils exposing (default)
import ButtonInput


-- Model definition
type LevelState = Default | Strike | DoubleStrike | CrossedOff


type alias Level = 
    (Int, LevelState)


type alias Player = 
    { name: String
    , levels: List Level
    }


type alias Model = 
    { players : Array Player
    , turn : Int
    , buttonInputModel : ButtonInput.Model
    }


initialModel : Model
initialModel = 
    { players = Array.fromList 
        [ Player "Player1" []
        , Player "Player2" []
        , Player "Player3" []
        , Player "Player4" []
        ]
    , turn = 0
    , buttonInputModel = ButtonInput.initialModel
    }


currentPlayer : Model -> Player
currentPlayer game = 
    let p = Array.get game.turn game.players in
        case p of
            Nothing -> Player "" []
            Just x -> x


nextTurn : Model -> Int
nextTurn game = (game.turn + 1) % Array.length game.players


validLevel : Level -> Bool
validLevel (_, state) =
    state == Default || state == Strike

currentScoreOf : Player -> Int
currentScoreOf = 
    .levels >> filter validLevel >> List.map Tuple.first >> head >> Utils.default 0