module ButtonInput exposing (..)

import Html exposing (Html, div, form, input, button, text)
import Html.Attributes exposing (class, placeholder, value, type_, autofocus)
import Html.Events exposing (onInput, onSubmit)

-- Model
type alias Model = String
initialModel : Model
initialModel = ""

-- Update
type Msg = UpdateField String | Submit String

update : Msg -> Model -> Model
update msg model = 
    case msg of 
        UpdateField s -> s
        Submit _ -> initialModel

-- View
buttonInput : String -> Model -> Html Msg
buttonInput p state = 
    form [ class "buttonInput row flex-center", onSubmit <| Submit state ]
        [ input 
            [ class "buttonInput col col-8 margin-small"
            , type_ "number"
            , placeholder p
            , onInput UpdateField
            , value state
            , autofocus True
            ] 
            []
        , button 
            [ type_ "submit"
            , class "btn btn-success col col-2 margin-small" 
            ] 
            [ text "Ok"] 
        ]