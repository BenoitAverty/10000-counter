#!/bin/bash

deployment=$(cd dist/ && now -p --static --name 10000 --token $NOW_TOKEN)
now alias $deployment $CI_ENVIRONMENT_URL --token $NOW_TOKEN