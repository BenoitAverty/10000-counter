module View exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import List exposing (reverse)
import Array exposing (Array, toList)

import Model exposing (..)
import Update exposing (Msg(..))
import ButtonInput exposing (buttonInput)


playerNameView : String -> Html Msg
playerNameView name = h1 [ class "text-success" ] [ text name ]


levelView : Level -> Html Msg
levelView level = 
    case level of
        (s, Default) -> li [ class "defaultScore" ] [ text (toString s) ]
        (s, Strike) -> li [ class "strikeScore text-danger" ] [ text (toString s), span [] [ text " ✘" ] ]
        (s, DoubleStrike) -> li [ class "strikeScore text-muted" ] [ del [] [ text (toString s), span [] [ text " ✘✘" ] ] ]
        (s, CrossedOff) -> li [ class "crossedOffScore text-muted" ] [ del [] [ text (toString s) ] ]


scoresView : List Level -> Html Msg
scoresView scores = 
    ul [ class "score" ] <| List.map levelView <| reverse scores


playerView : Player -> Html Msg
playerView p = 
    div [class "player padding-small"]
        [ playerNameView p.name
        , scoresView <| reverse p.levels
        ]


playersView : Array Player -> Html Msg
playersView players = div [ id "players", class "row flex-spaces" ] <| List.map playerView <| toList players


view : Model -> Html Msg
view game =
    div [ id "game" ]
        [ Html.map ButtonInputMsg <| ButtonInput.buttonInput ((currentPlayer game).name ++ "'s score...") game.buttonInputModel 
        , playersView game.players
        ]
